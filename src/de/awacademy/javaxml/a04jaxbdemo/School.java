package de.awacademy.javaxml.a04jaxbdemo;

import de.awacademy.javaxml.a04jaxbdemo.Student;
import de.awacademy.javaxml.a04jaxbdemo.Teacher;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class School {

    @XmlAttribute
    private String name;

    @XmlElementWrapper(name="teachers")
    @XmlElement(name = "teacher")
    private List<Teacher> teacherList;

    @XmlElementWrapper(name="students")
    @XmlElement(name = "student")
    private List<Student> studentList;


    public School() {
        this.teacherList = new ArrayList<>();
        this.studentList = new ArrayList<>();
    }

    public School(String name) {
        this();
        this.name = name;
    }

    public void addTeacher(Teacher teacher) {
        teacherList.add(teacher);
    }

    public void addStudent(Student student) {
        studentList.add(student);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    @Override
    public String toString() {
        return "School{" +
                "name='" + name + '\'' +
                ", teacherList=" + teacherList +
                ", studentList=" + studentList +
                '}';
    }
}

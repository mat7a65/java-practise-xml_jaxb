package de.awacademy.javaxml.a04jaxbdemo;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class A04_JAXBDemo {

    private static final String FILENAME = "school.xml";


    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        new A04_JAXBDemo().run();
    }

    private void run() throws JAXBException, FileNotFoundException {
        School school = createSchool();
        writeSchoolToXML(school, FILENAME);

        School schoolRed = readSchoolFromXML(FILENAME);
        System.out.println(schoolRed);
    }

    private School createSchool() {
        School school = new School("Online-Schule");
        school.addTeacher(new Teacher("Müller"));
        school.addTeacher(new Teacher("Meier"));
        school.addStudent(new Student("Anton"));
        school.addStudent(new Student("Berta"));
        return school;
    }

    private void writeSchoolToXML(School school, String filename) throws JAXBException, FileNotFoundException {
        JAXBContext jaxbContext = JAXBContext.newInstance(School.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(school, new FileOutputStream(filename));
    }

    private School readSchoolFromXML(String filename) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(School.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        School school = (School) unmarshaller.unmarshal(new File(filename));
        return school;
    }
}

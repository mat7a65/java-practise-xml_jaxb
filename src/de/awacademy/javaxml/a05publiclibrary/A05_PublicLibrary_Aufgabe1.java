package de.awacademy.javaxml.a05publiclibrary;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class A05_PublicLibrary_Aufgabe1 {
    // Aufgabe 1: Schreibe ein Programm, welches:
    //                  die XML-Datei public_library.xml mit Hilfe von JAXB einliest und
    //                  in ein Java-Objektmodell verwandelt
    //                  Anschließend füge dem Java-Objektmodell einen Datensatz für eine Ausleihe hinzu
    //                  Speichere das Java-Objektmodell mit Hilfe von JAXB wieder in einer XML-Datei

    private static final String FILENAME = "public_library.xml";

    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        new A05_PublicLibrary_Aufgabe1().run();
    }

    private void run() throws JAXBException, FileNotFoundException {
        Library library = readLibraryFromXML(FILENAME);

        Customer customer = library.getCustomerList().get(1);
        Book returnBook = library.getBookList().get(0);
        Book loanBook = library.getBookList().get(1);

        library.returnBook(customer, returnBook);
        library.loanBook(customer, loanBook);

        writeToXML(library, FILENAME);
    }

    private Library readLibraryFromXML(String filename) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Library library = (Library) unmarshaller.unmarshal(new File(filename));
        return library;
    }

    private void writeToXML(Library library, String filename) throws JAXBException, FileNotFoundException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(library, new FileOutputStream(filename));
    }
}

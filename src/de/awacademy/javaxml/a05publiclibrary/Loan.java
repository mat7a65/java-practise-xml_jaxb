package de.awacademy.javaxml.a05publiclibrary;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class Loan {

    @XmlAttribute
    private int bookId;
    @XmlAttribute
    private int customerId;

    public Loan() {
    }

    public Loan(int bookId, int customerId) {
        this.bookId = bookId;
        this.customerId = customerId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getCustomerId() {
        return customerId;
    }


    @Override
    public String toString() {
        return "Loan{" +
                "bookId=" + bookId +
                ", customerId=" + customerId +
                '}';
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}

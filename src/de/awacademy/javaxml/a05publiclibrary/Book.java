package de.awacademy.javaxml.a05publiclibrary;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;


@XmlAccessorType(XmlAccessType.FIELD)
public class Book {

    @XmlAttribute
    private String title;
    @XmlAttribute
    private String isbn;
    @XmlAttribute
    private String publisher;
    @XmlAttribute
    private String author;
    @XmlAttribute
    private int id;

    public Book() {
    }

    public Book(String title, String isbn, String publisher, String author, int id) {
        this.title = title;
        this.isbn = isbn;
        this.publisher = publisher;
        this.author = author;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", isbn='" + isbn + '\'' +
                ", publisher='" + publisher + '\'' +
                ", author='" + author + '\'' +
                ", id=" + id +
                '}';
    }
}

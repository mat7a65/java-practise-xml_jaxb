package de.awacademy.javaxml.a05publiclibrary;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "library")
@XmlAccessorType(XmlAccessType.FIELD)
public class Library {

    @XmlElementWrapper(name = "books")
    @XmlElement(name = "book")
    private List<Book> bookList;

    @XmlElementWrapper(name = "examples")
    @XmlElement(name = "example")
    private List<Example> exampleList;

    @XmlElementWrapper(name = "customers")
    @XmlElement(name = "customer")
    private List<Customer> customerList;

    @XmlElementWrapper(name = "loans")
    @XmlElement(name = "loan")
    private List<Loan> loanList;

    public Library() {
        this.bookList = new ArrayList<>();
        this.exampleList = new ArrayList<>();
        this.customerList = new ArrayList<>();
        this.loanList = new ArrayList<>();
    }

    public void returnBook(Customer customer, Book book) {
        loanList.stream()
                .filter(x -> x.getCustomerId() == customer.getId() && x.getBookId() == book.getId())
                .findFirst()
                .ifPresent(x -> loanList.remove(x));

        exampleList.stream()
                .filter(x -> x.getBookId() == book.getId())
                .findFirst()
                .ifPresent(example -> example.returnBook());
    }

    public void loanBook(Customer customer, Book book) {
        loanList.add(new Loan(book.getId(), customer.getId()));
        exampleList.stream()
                .filter(x -> x.getBookId() == book.getId())
                .findFirst()
                .ifPresent(example -> example.loanOne());
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    public List<Example> getExampleList() {
        return exampleList;
    }

    public void setExampleList(List<Example> exampleList) {
        this.exampleList = exampleList;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public List<Loan> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<Loan> loanList) {
        this.loanList = loanList;
    }
}

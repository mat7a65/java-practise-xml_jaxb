package de.awacademy.javaxml.a05publiclibrary;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class Example {

    @XmlAttribute
    private int bookId;
    @XmlAttribute
    private int availableCount;
    @XmlAttribute
    private int exampleCount;

    public Example() {
    }

    public Example(int availableCount, int exampleCount) {
        this.bookId = bookId;
        this.availableCount = availableCount;
        this.exampleCount = exampleCount;
    }

    public void loanOne() {
        this.availableCount--;
    }

    public void returnBook() {
        this.availableCount++;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getExampleCount() {
        return exampleCount;
    }

    public void setExampleCount(int exampleCount) {
        this.exampleCount = exampleCount;
    }

    public int getAvailableCount() {
        return availableCount;
    }

    public void setAvailableCount(int availableCount) {
        this.availableCount = availableCount;
    }

}
